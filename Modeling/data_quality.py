# -*- coding: utf-8 -*-
""" Initial python file for fixing data quality issues. Should run first
Author : Strahinja Ivanovic
Date : 23th September 2016
"""

import csv as csv
import numpy as np

csv_file_object = csv.reader(open('/home/ivan/Dokumente/Kaggle_Data_Science/Data/train.csv', 'rb'))
header = csv_file_object.next()
data=[]

for row in csv_file_object:
    data.append(row)
data = np.array(data)

Scan = data[0::,0:3].astype(np.integer)

with open("NewCSV.csv", "wb") as c:

    np.savetxt(c, Scan, delimiter=",")